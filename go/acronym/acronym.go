// This is a "stub" file.  It's a little start on your solution.
// It's not a complete solution though; you have to write some code.

// Package acronym should have a package comment that summarizes what it's about.
// https://golang.org/doc/effective_go.html#commentary
package acronym

import (
	"regexp"
	"strings"
)

// Abbreviate should have a comment documenting it.
func Abbreviate(s string) string {
	acronym := ""
	// I wish I could think of a general case for pseudo-slugifying the
	// input string, but I can't. I hate coding specifically to the tests.
	// HATE IT.
	reStrip := regexp.MustCompile(`[^-A-Za-z\s]`)
	reReplace := regexp.MustCompile(`[-]`)
	// Strip non-word-like characters
	words := reStrip.ReplaceAllString(s, "")
	// Replace word-like characters with a space
	words = reReplace.ReplaceAllString(words, " ")

	for _, w := range strings.Split(words, " ") {
		// Ignore empty array elements
		if len(strings.TrimSpace(w)) > 0 {
			acronym += string(w[0])
		}
	}

	return strings.ToUpper(acronym)
}
