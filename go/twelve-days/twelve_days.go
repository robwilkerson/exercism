package twelve

import (
	"fmt"
	"strings"
)

const DaysOfChristmas = 12

var ordinals = []string{
	"first",
	"second",
	"third",
	"fourth",
	"fifth",
	"sixth",
	"seventh",
	"eighth",
	"ninth",
	"tenth",
	"eleventh",
	"twelfth",
}
var gifts = []string{
	"twelve Drummers Drumming",
	"eleven Pipers Piping",
	"ten Lords-a-Leaping",
	"nine Ladies Dancing",
	"eight Maids-a-Milking",
	"seven Swans-a-Swimming",
	"six Geese-a-Laying",
	"five Gold Rings",
	"four Calling Birds",
	"three French Hens",
	"two Turtle Doves",
	"a Partridge in a Pear Tree",
}

// Song generates song by compiling its verses
func Song() string {
	var song []string

	for i := 1; i <= DaysOfChristmas; i++ {
		song = append(song, Verse(i))
	}

	return strings.Join(song, "\n")
}

// Verse generates each individual verse of the song
// d is the day value (1-12)
func Verse(d int) string {
	// adjust for the 0-based array index
	i := d - 1
	// create a copy of the gifts slice to avoid mutating the original
	tmp := make([]string, len(gifts))
	copy(tmp, gifts)
	// snip only the gifts we need to fill the verse for a given day
	g := tmp[len(tmp)-d:]

	// slap an "and" before the final item in a list of gifts
	if len(g) > 1 {
		lastIndex := len(g) - 1
		g[lastIndex] = fmt.Sprintf("and %s", g[lastIndex])
	}

	return fmt.Sprintf(
		"On the %s day of Christmas my true love gave to me: %v.",
		ordinals[i],
		strings.Join(g, ", "),
	)
}
