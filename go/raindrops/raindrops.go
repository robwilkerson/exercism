package raindrops

import "strconv"

// Convert ...
func Convert(i int) string {
	sound := ""

	if i%3 == 0 {
		sound += "Pling"
	}
	if i%5 == 0 {
		sound += "Plang"
	}
	if i%7 == 0 {
		sound += "Plong"
	}

	if len(sound) == 0 {
		sound = strconv.Itoa(i)
	}

	return sound
}
