package luhn

import (
	"regexp"
	"strconv"
	"strings"
)

func double(n int64) int64 {
	doubled := n * 2

	if doubled > 9 {
		doubled -= 9
	}

	return doubled
}

func Valid(n string) bool {
	clean := strings.ReplaceAll(n, " ", "")
	reValid := regexp.MustCompile(`^[0-9]{2,}$`)

	// Exit early if non-numerics are present after spaces are stripped
	if !reValid.Match([]byte(clean)) {
		return false
	}

	var sum int64 = 0
	doubleIt := false

	for i := len(clean) - 1; i >= 0; i-- {
		// converts the character to its int8 value
		digit, err := strconv.ParseInt(string(clean[i]), 10, 8)
		if err != nil {
			return false
		}

		if doubleIt {
			digit = double(digit)
		}

		sum += digit

		// double every second digit
		doubleIt = !doubleIt
	}

	return sum%10 == 0
}
