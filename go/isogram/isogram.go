package isogram

import (
	"strings"
	"unicode"
)

// IsIsogram ...
func IsIsogram(s string) bool {
	normalized := strings.ToUpper(s)
	chars := map[rune]int{}

	for _, char := range normalized {
		if unicode.IsLetter(char) {
			if _, ok := chars[char]; ok {
				return false
			}
		}

		chars[char] = 1
	}

	return true
}
