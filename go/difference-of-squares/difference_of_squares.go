package diffsquares

import "math"

// SquareOfSum calculates the square of the sum of the first n postitive
// numbers. Formula derived from
// https://brilliant.org/wiki/sum-of-n-n2-or-n3/#sum-of-the-first-n-positive-integers
func SquareOfSum(n int) int {
	return int(math.Pow(float64(n*(n+1)/2), 2))
}

// SumOfSquares calculates the sum of the squares of the first n postitive
// numbers. Formula from
// https://brilliant.org/wiki/sum-of-n-n2-or-n3/#sum-of-the-squares-of-the-first-n-positive-integers
func SumOfSquares(n int) int {
	return (n * (n + 1) * ((2 * n) + 1)) / 6
}

// Difference calculates the difference between the square of the sum and the
// sum of the squares of the first n natural numbers
func Difference(n int) int {
	return SquareOfSum(n) - SumOfSquares(n)
}
