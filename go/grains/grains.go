package grains

import (
	"errors"
)

func Square(s int) (uint64, error) {
	if s < 1 || s > 64 {
		return 0, errors.New("invalid square")
	}

	exponent := s - 1
	square := uint64(1 << exponent)

	return square, nil
}

func Total() uint64 {
	var sum uint64 = 0

	for i := 1; i <= 64; i++ {
		sq, _ := Square(int(i))

		sum += sq
	}

	return sum
}
