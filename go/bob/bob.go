// This is a "stub" file.  It's a little start on your solution.
// It's not a complete solution though; you have to write some code.

// Package bob should have a package comment that summarizes what it's about.
// https://golang.org/doc/effective_go.html#commentary
package bob

import (
	"strings"
)

// Hey should have a comment documenting it.
func Hey(remark string) string {
	// trim the incoming remark before testing it
	hey := strings.TrimSpace(remark)

	// r is Bob's response
	r := ""
	// isQuestion attempts to recognize a question by the punctuation
	isQuestion := strings.HasSuffix(hey, "?")
	// isShout determines whether the remark is a shout
	isShout := hey == strings.ToUpper(hey) && strings.ToUpper(hey) != strings.ToLower(hey)

	switch {
	case len(hey) == 0:
		r = "Fine. Be that way!"
	case isQuestion && isShout:
		r = "Calm down, I know what I'm doing!"
	case isQuestion:
		r = "Sure."
	case !isQuestion && isShout:
		r = "Whoa, chill out!"
	default:
		r = "Whatever."
	}

	return r
}
