package reverse

import (
	"unicode/utf8"
)

// Reverse ...
func Reverse(s string) string {
	// convert the string to an array of runes for iteraction
	runes := []rune(s)
	// handle unicode chars when dealing with length
	runeLen := utf8.RuneCountInString(string(s))

	for i, j := 0, runeLen-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}

	return string(runes)
}
