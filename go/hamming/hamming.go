package hamming

import (
	"errors"
)

// Distance ...
func Distance(a, b string) (int, error) {
	var err error
	hammingDistance := 0
	compare := []rune(b)

	if len(a) != len(b) {
		return 0, errors.New("Sequences are not of equal length")
	}

	for k, v := range a {
		if compare[k] != v {
			hammingDistance++
		}
	}

	return hammingDistance, err
}
