package space

// Planet is ...
type Planet string

// secondsInAYear is the number of seconds in an "earth year"
const secondsInAYear = 31557600

// OrbitalPeriod maps planets to their "earth year" multiplier
var OrbitalPeriod = map[string]float64{
	"Mercury": 0.2408467,
	"Venus":   0.61519726,
	"Earth":   1.0,
	"Mars":    1.8808158,
	"Jupiter": 11.862615,
	"Saturn":  29.447498,
	"Uranus":  84.016846,
	"Neptune": 164.79132,
}

// Age is ...
func Age(s float64, p Planet) float64 {
	earthAge := s / secondsInAYear

	return earthAge / OrbitalPeriod[string(p)]
}
